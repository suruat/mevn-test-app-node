const Router = require('express').Router;
const tasksController = require('../resources/tasks/tasks.controller.js');
const { taskValidation } = require('../utils/tasks.js');

const router = Router();

router
	.route('/tasks')
	.get(tasksController.getTasks)
	.post(taskValidation, tasksController.createTask);

router
	.route('/tasks/:id')
	.put(taskValidation, tasksController.updateTask)
	.delete(tasksController.deleteTask);

module.exports = router;
