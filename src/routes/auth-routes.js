const Router = require('express').Router;
const { body } = require('express-validator');

const usersController = require('../resources/users/users.controller.js');
const User = require('../resources/users/users.model.js');

const router = Router();

router.post(
	'/register',
	[
		body('email', 'Invalid email')
			.isEmail()
			.normalizeEmail()
			.custom(async email => {
				const user = await User.findOne({email});
				if(user) {
					throw new Error('User with this email already exist');
				}

				return true;
			}),
		body('password', 'Password should be at least 6 characters long')
			.trim()
			.isLength({min: 6}),
		body('repeatPassword', 'Passwords should match!')
			.custom((repeatPassword, {req}) => {
				if (repeatPassword !== req.body.password) {
					throw new Error();
				}

				return true;
			})
	],
	usersController.registerUser
);
router.post(
	'/login',
	[
		body('email')
			.isEmail()
			.normalizeEmail()
			.custom(async (email, {req}) => {
				const errorMsg = "Wrong email and/or password";
				const user = await User.findOne({ email });

				if (!user) {
					throw new Error(errorMsg);
				}

				const match = await user.checkPassword(req.body.password);
				if (!match) {
					throw new Error(errorMsg);
				}

				return true;
			}),
	],
	usersController.login
);

module.exports = router;
