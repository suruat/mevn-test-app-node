const { body, validationResult } = require('express-validator');

exports.taskValidation = async function (req, res, next) {
	console.log(req.body);

	const validations = [
		body('description')
			.trim()
			.not().isEmpty()
			.withMessage('Description should not be empty')
			.isLength({max: 3000})
			.withMessage('Description should not be longer than 3000 characters')
			.escape(),
		body('dueDate', 'Invalid Date')
			.isISO8601()
	];

	await Promise.all(validations.map(validation => validation.run(req)));

	let errors = validationResult(req);
	if (errors.isEmpty()) {
		return next();
	}

	errors = errors.array().reduce((acc, err) => {
		acc[err.param] = err.msg
		return acc;
	}, {});

	res.status(422).json({ errors });
}
