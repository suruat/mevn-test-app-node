const mongoose = require('mongoose');
const dbUrl = require('../configs/dev.js').dbUrl;

module.exports = (url = dbUrl, opts = {}) => {
	return mongoose.connect(
		url,
		{
			...opts,
			useNewUrlParser: true,
			useFindAndModify: false
		}
	)
}
