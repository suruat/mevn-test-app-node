const jwt =  require('jsonwebtoken');
const {validationResult} = require('express-validator');

const config =  require('../../configs/dev.js');
const User =  require('./users.model.js');

module.exports = {
	async registerUser (req, res) {
		const { email, password } = req.body;
		let errors = validationResult(req);

		if (!errors.isEmpty()) {
			errors = errors.array().reduce((acc, err) => {
				acc[err.param] = err.msg
				return acc;
			}, {});

			return res.status(406).json({ errors });
		}

		try {
			await User.create({
				email,
				password
			});

			return res.status(201).json({ success: true });
		} catch (err) {
			res.status(406).send({error: err});
		}
	},

	async login(req, res) {
		let errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(403).json(
				{
					errors: {
						general: errors.array()[0].msg
					}
				}
			);
		}

		try {
			const { email } = req.body;
			const user = await User.findOne({ email });

			const token = jwt.sign(
				{id: user.id},
				config.jwt.secret,
				{ expiresIn: config.jwt.expires }
			);
			const userPublicData = {
				email: user.email,
				username: user.username
			}
			return res.status(201).send({ data: {user: userPublicData, token} });

		} catch (err) {
			return res.status(401).send({ message: err.message });
		}
	}
}
