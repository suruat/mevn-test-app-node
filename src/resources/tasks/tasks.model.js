const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
	description: {
		type: String,
		required: true,
		trim: true
	},
	dueDate: {
		type: Date,
		required: true
	},
	createdBy: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'users',
		required: true
	}
})

module.exports = mongoose.model('Task', TaskSchema);
