const Task = require('./tasks.model.js');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports = {
	async getTasks(req, res) {
		try {
			const tasks = await Task
				.find({createdBy: req.user._id})
				.select('-createdBy')
				.exec();

			res.status(200).json({data: {tasks}});
		} catch (err) {
			console.error(err);
			return res.status(400).send({ message: err.message });
		}
	},

	async createTask(req, res) {
		const { description, dueDate } = req.body;

		const createdBy = req.user._id;

		try{
			const newTask = await Task.create({
				description,
				dueDate,
				createdBy
			});

			return res.status(200).json({data: {newTask}});
		} catch (err) {
			console.error(err);
			return res.status(400).send({ message: err.message });
		}
	},

	async updateTask(req, res) {
		const { description, dueDate } = req.body;
		const taskID = req.params.id;

		if (!ObjectId.isValid(taskID)){
			throw new Error('ID is invalid');
		}

		try {
			const updatedTask = await Task.findOneAndUpdate(
				{
					_id: taskID,
					createdBy: req.user._id
				},
				{
					description,
					dueDate
				},
				{
					new: true,
					lean: true
				}
			).select('-createdBy');

			return res.status(200).json({ data: {updatedTask} });
		} catch (err) {
			console.error(err);
			return res.status(400).send({ message: err.message });
		}
	},

	async deleteTask(req, res) {
		try {
			const taskID = req.params.id;

			if (!ObjectId.isValid(taskID)) {
				throw new Error('ID is invalid');
			}

			const deletedTask = await Task.findOneAndRemove({
				_id: taskID,
				createdBy: req.user._id
			}).select('-createdBy');

			return res.status(200).json({ data: {deletedTask} });
		} catch (err) {
			console.error(err);
			return res.status(400).send({ message: err.message });
		}
	}
}
