const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');

const config = require('./configs/dev.js');
const connectDB = require('./utils/db.js');
const protect = require('./utils/auth.js').protect;

// Routes Imports
const authRoutes = require('./routes/auth-routes.js');
const apiRoutes = require('./routes/api-routes.js');

const app = express();

app.disable('x-powered-by')

// Middlewares
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function setCORSHeaders (req, res, next) {
	res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
}
// Routes
app.use('/auth', setCORSHeaders, authRoutes);
app.use('/api', cors(), protect, apiRoutes);

(async _ => {
	try{
		await connectDB();

		app.listen(config.port, _ => {
			console.log(`Server is running on port ${config.port}`);
			console.log(`REST API is available on http://localhost:${config.port}/api`);
			console.log(`Auth endpoints are available on http://localhost:${config.port}/auth`);
		});
	} catch (err) {
		console.error(err);
	}
})();
