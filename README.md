# Back-end part of Test App

API for test Task App written with Express.js


## Installation and Launch

```bash
git clone https://gitlab.com/suruat/mevn-test-app-node.git

cd mevn-test-app-node

npm install

npm start
```

